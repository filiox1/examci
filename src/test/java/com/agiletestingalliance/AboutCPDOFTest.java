package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;
		
public class AboutCPDOFTest{

	@Test
	public void testAbout()
	{
	   AboutCPDOF abt = new AboutCPDOF(); 
	   assertTrue("Page contains some of the about text", abt.desc().contains("certification program"));
	}
}
