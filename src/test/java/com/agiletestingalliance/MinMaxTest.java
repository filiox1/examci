package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;
		
public class MinMaxTest{

	@Test
	public void testMax()
	{
	   MinMax minMax = new MinMax();
	   assertEquals("Max", 4,minMax.getMinMax(2,4));
	}
	@Test
	public void testMin()
	{
	   MinMax minMax = new MinMax();
	   assertEquals("Min", 4, minMax.getMinMax(4,2));
	}
	@Test
	public void testBar()
	{
	   MinMax minMax = new MinMax();
	   assertEquals("Bar", minMax.bar(null), null);
	}
	@Test
	public void testBar2()
	{
	   MinMax minMax = new MinMax();
	   assertEquals("Bar", minMax.bar("ggg"), "ggg");
	}
	@Test
	public void testBar3()
	{
	   MinMax minMax = new MinMax();
	   assertEquals("Bar", minMax.bar(""), "");
	}
}
