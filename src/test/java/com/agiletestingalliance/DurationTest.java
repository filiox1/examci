package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;
		
public class DurationTest{

	@Test
	public void testDesc()
	{
	   Duration dur = new Duration(); 
	   assertTrue("Page contains some of the about text", dur.dur().contains("working professionals"));
	}
}
